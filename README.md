# [log.samuel.ortion.fr](https://log.samuel.ortion.fr/)

Repository for my web logs, powered by [Quarto](https://quarto.org/).

Dual licensed blog: [CC By-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/) and [MIT License](https://opensource.org/licenses/MIT) for content and source code.
